package ch.ffhs.dua.permutations;

import java.util.Arrays;
import ch.ffhs.ctsoft.helpers.Logger;
import ch.ffhs.ctsoft.helpers.Math;

public class Permutations
{
    private static int permutationsIndex = 0;
    private static int[][] permutations;
    
    public static void addPermutation(int[] permutation)
    {
        permutations[permutationsIndex] = permutation;
        permutationsIndex++;
    }
    
    public static int[][] getPermutations()
    {
        return permutations;
    }
    /**
     * Erzeugt ein Array von allen Permutationen von {0,1,2,3,...,n-1}.
     *
     * @param n Anzahl Elemente in einer Permutation.
     * @return Ein Array von Permutationen; jede Permutation ist ein Array von
     * Integern.
     */
    public static int[][] permutations(int n)
    {
        int factorial = Math.factorial(n);
        int countPermutations = 0;
        if (n > 0) {
            countPermutations = Math.factorial(n);
        }
        
        System.out.println("PVA 1 - Aufgabe 2 - Permutation von Natürlichen Zahlen");
        System.out.println("Anzahl Elemente pro Permutation: " + n);
        System.out.println("Anzahl Permutationen: " + countPermutations);
        
        int[] startArr = Math.rangeAsArrayInt(n);
        permutations = new int[countPermutations][n];
        permutationsIndex = 0;
        
        permute(startArr, 0);
        
        int[][] perms = getPermutations();
        Logger logger = Logger.getInstance();
        logger.logArray3D(perms);

        return perms;
    }

    public static void main(String args[])
    {
        permutations(4);
    }

    /* https://stackoverflow.com/questions/2920315/permutation-of-array#answer-14444037 */
    private static void permute(int[] arr, int k)
    {
        for (int i = k; i < arr.length; i++) {
            swap(arr, i, k);
            permute(arr, k+1);
            swap(arr, k, i);
        }
        if (k == arr.length-1){
            addPermutation(arr);
        }
    }
    
    private static void swap(int[] arr, int i, int j)
    {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
