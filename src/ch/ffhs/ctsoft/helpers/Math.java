package ch.ffhs.ctsoft.helpers;

import java.util.stream.IntStream;

public class Math
{
    public static int factorial(int n)
    {
        return n == 0 ? 1 : n * factorial(n-1);
    }
    
    public static int[] rangeAsArrayInt(int n)
    {
        return IntStream.range(0, n).toArray();
    }
}
