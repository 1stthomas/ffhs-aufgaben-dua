package ch.ffhs.ctsoft.helpers;

import java.util.Arrays;

public class Logger
{
    private static Logger instance;
    private String destination = "console";
    
    public Logger()
    {
        
    }
    
    public static Logger getInstance()
    {
        if (instance == null) {
            instance = new Logger();
        }
        
        return instance;
    }
    
    public void logArray3D(int[][] arr)
    {
        for (int i=0; i<arr.length; i++) {
            System.out.println("row " + i + " - value: " + Arrays.toString(arr[i]));
        }
    }
    
    public static void logArray3DStatic(int[][] arr)
    {
        for (int i=0; i<arr.length; i++) {
            System.out.println("row " + i + " - value: " + Arrays.toString(arr[i]));
        }
    }
}
